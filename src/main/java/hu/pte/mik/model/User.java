package hu.pte.mik.model;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "t_user")
public class User implements UserDetails {

	private static final long serialVersionUID = 9104418792628048163L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer userId;

	@Column(name = "username", nullable = false)
	private String username;

	@Column(name = "password", nullable = false)
	private String password;

	@Column(name = "email", nullable = false)
	private String email;

	@Column(name = "account_non_expired", nullable = false)
	private boolean accountNonExpired;

	@Column(name = "account_non_locked", nullable = false)
	private boolean accountNonLocked;

	@Column(name = "credentials_non_expired", nullable = false)
	private boolean credentialsNonExpired;

	@Column(name = "enabled", nullable = false)
	private boolean enabled;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "t_user_role", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	private Set<UserRole> roles = new HashSet<>();

	@Column(name = "place_of_birth", nullable = false)
	private String placeOfBirth;

	@Column(name = "date_of_birth", nullable = false)
	private Date dateOfBirth;

	@Column(name = "nationality", nullable = false)
	private String nationality;

	@Column(name = "phone_number", nullable = false)
	private String phoneNumber;

	@Column(name = "created_on", nullable = false)
	private Date createdOn;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	private Set<Study> studySet;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	private Set<Work> workSet;

	public User() {
	}

	public User(String username, String password, String email, UserRole userRole, String placeOfBirth,
			Date dateOfBirth, String nationality, String phoneNumber) {
		this(username, password, email, true, true, true, true, userRole, placeOfBirth, dateOfBirth, nationality,
				phoneNumber, new Date());
	}

	public User(String username, String password, String email, boolean accountNonExpired, boolean accountNonLocked,
			boolean credentialsNonExpired, boolean enabled, UserRole userRole, String placeOfBirth, Date dateOfBirth,
			String nationality, String phoneNumber, Date createdOn) {
		this.username = username;
		this.password = password;
		this.email = email;
		this.accountNonExpired = accountNonExpired;
		this.accountNonLocked = accountNonLocked;
		this.credentialsNonExpired = credentialsNonExpired;
		this.enabled = enabled;
		this.roles.add(userRole);
		this.placeOfBirth = placeOfBirth;
		this.dateOfBirth = dateOfBirth;
		this.nationality = nationality;
		this.phoneNumber = phoneNumber;
		this.createdOn = createdOn;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.unmodifiableCollection(this.roles);
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean isAccountNonExpired() {
		return this.accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return this.credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public String getEmail() {
		return this.email;
	}

	public String getPlaceOfBirth() {
		return this.placeOfBirth;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public String getNationality() {
		return this.nationality;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public Set<Study> getStudySet() {
		return this.studySet;
	}

	public void addStudy(Study study) {
		this.studySet.add(study);
	}

	public Set<Work> getWorkSet() {
		return this.workSet;
	}

	public void addWork(Work work) {
		this.workSet.add(work);
	}

	public void setStudySet(Set<Study> studySet) {
		this.studySet = studySet;
	}

	public void setWorkSet(Set<Work> workSet) {
		this.workSet = workSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((this.email == null) ? 0 : this.email.hashCode());
		result = (prime * result) + ((this.username == null) ? 0 : this.username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		User other = (User) obj;
		if (this.email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!this.email.equals(other.email)) {
			return false;
		}
		if (this.username == null) {
			if (other.username != null) {
				return false;
			}
		} else if (!this.username.equals(other.username)) {
			return false;
		}
		return true;
	}

}

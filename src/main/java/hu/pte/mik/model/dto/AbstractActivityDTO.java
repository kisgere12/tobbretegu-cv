package hu.pte.mik.model.dto;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

public abstract class AbstractActivityDTO {

	private Integer id;

	@NotEmpty(message = "Mező töltése kötelező")
	private String name;

	@NotNull(message = "Mező töltése kötelező")
	@Past(message = "A tanulmány kezdete csak a múltban lehet")
	@DateTimeFormat(pattern = "yyyy.MM.dd")
	private Date startDate;

	@NotNull(message = "Mező töltése kötelező")
	@Past(message = "A tanulmány vége csak a múltban lehet")
	@DateTimeFormat(pattern = "yyyy.MM.dd")
	private Date endDate;

	public AbstractActivityDTO() {
	}

	public AbstractActivityDTO(Integer id, String name, Date startDate, Date endDate) {
		this.id = id;
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}

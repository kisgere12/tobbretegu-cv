package hu.pte.mik.model.dto;

import javax.validation.constraints.NotNull;

import hu.pte.mik.model.Study;

public class StudyDTO extends AbstractActivityDTO {

	@NotNull(message = "Mező töltése kötelező")
	private String result;

	public StudyDTO() {
		super();
	}

	public StudyDTO(Study study) {
		super(study.getId(), study.getName(), study.getStartDate(), study.getEndDate());
		this.result = study.getResult();
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}

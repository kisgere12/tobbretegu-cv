package hu.pte.mik.model.dto;

import java.util.Date;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

public class UserRegistrationDTO {

	@NotEmpty(message = "Mező töltése kötelező")
	private String username;

	@NotEmpty(message = "Mező töltése kötelező")
	private String password;

	@NotEmpty(message = "Mező töltése kötelező")
	private String confirmPassword;

	@Email(message = "Nem megfelelő email formátum")
	@NotEmpty(message = "Mező töltése kötelező")
	private String email;

	@Email(message = "Nem megfelelő email formátum")
	@NotEmpty(message = "Mező töltése kötelező")
	private String confirmEmail;

	@NotEmpty(message = "Mező töltése kötelező")
	private String phoneNumber;

	@NotEmpty(message = "Mező töltése kötelező")
	private String placeOfBirth;

	@NotNull(message = "Mező töltése kötelező")
	@Past(message = "Születési idő csak a múltban lehet")
	@DateTimeFormat(pattern = "yyyy.MM.dd")
	private Date dateOfBirth;

	@NotEmpty(message = "Mező töltése kötelező")
	private String nationality;

	@AssertTrue(message = "Felhasználási feltételek elfogadása kötelező")
	private Boolean terms;

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return this.confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getConfirmEmail() {
		return this.confirmEmail;
	}

	public void setConfirmEmail(String confirmEmail) {
		this.confirmEmail = confirmEmail;
	}

	public Boolean getTerms() {
		return this.terms;
	}

	public void setTerms(Boolean terms) {
		this.terms = terms;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPlaceOfBirth() {
		return this.placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

}

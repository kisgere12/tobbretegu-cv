package hu.pte.mik.model.dto;

import javax.validation.constraints.NotNull;

import hu.pte.mik.model.Work;

public class WorkDTO extends AbstractActivityDTO {

	@NotNull(message = "Mező töltése kötelező")
	private String position;

	@NotNull(message = "Mező töltése kötelező")
	private String task;

	public WorkDTO() {
		super();
	}

	public WorkDTO(Work work) {
		super(work.getId(), work.getName(), work.getStartDate(), work.getEndDate());
		this.position = work.getPosition();
		this.task = work.getTask();
	}

	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTask() {
		return this.task;
	}

	public void setTask(String task) {
		this.task = task;
	}


}

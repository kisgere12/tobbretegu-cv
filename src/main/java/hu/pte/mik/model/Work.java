package hu.pte.mik.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "t_work")
@Entity
public class Work extends AbstractActivity {

	private static final long serialVersionUID = -1187147725942201468L;

	@Column(name = "position", nullable = false)
	private String position;

	@Column(name = "task", nullable = false, columnDefinition = "CLOB")
	private String task;

	public Work() {
		super();
	}

	public Work(String name, Date startDate, Date endDate, User user, String position, String task) {
		this(null, name, startDate, endDate, user, position, task);
	}

	public Work(Integer id, String name, Date startDate, Date endDate, User user, String position, String task) {
		super(id, name, startDate, endDate, user);
		this.position = position;
		this.task = task;
	}

	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTask() {
		return this.task;
	}

	public void setTask(String task) {
		this.task = task;
	}

}

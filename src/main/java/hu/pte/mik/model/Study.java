package hu.pte.mik.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "t_study")
@Entity
public class Study extends AbstractActivity {

	private static final long serialVersionUID = 5105082027118395537L;

	@Column(name = "result", nullable = false)
	private String result;

	public Study() {
		super();
	}

	public Study(String name, Date startDate, Date endDate, User user, String result) {
		this(null, name, startDate, endDate, user, result);
	}

	public Study(Integer id, String name, Date startDate, Date endDate, User user, String result) {
		super(id, name, startDate, endDate, user);
		this.result = result;
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}

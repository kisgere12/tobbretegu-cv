package hu.pte.mik.model.menu;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import hu.pte.mik.model.UserRole;

@Entity
@Table(name = "t_menu")
public class MenuItem implements Serializable {

	private static final long serialVersionUID = -234715584393775114L;

	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "url", nullable = false)
	private String url;

	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "parent_menu_id")
	private MenuItem parentMenuItem;

	@OneToMany(mappedBy = "parentMenuItem", fetch = FetchType.EAGER)
	private List<MenuItem> subMenuList;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "t_menu_role", joinColumns = @JoinColumn(name = "menu_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	private List<UserRole> role;

	@Enumerated(EnumType.STRING)
	@Column(name = "menu_type", nullable = false)
	private MenuType menuType;

	@Column(name = "menu_order", nullable = false)
	private Integer order;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<MenuItem> getSubMenuList() {
		return this.subMenuList;
	}

	public void setSubMenuList(List<MenuItem> subMenuList) {
		this.subMenuList = subMenuList;
	}

	public List<UserRole> getRole() {
		return this.role;
	}

	public void setRole(List<UserRole> role) {
		this.role = role;
	}

	public MenuType getMenuType() {
		return this.menuType;
	}

	public void setMenuType(MenuType menuType) {
		this.menuType = menuType;
	}

	public Integer getOrder() {
		return this.order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

}

package hu.pte.mik.model.menu;

/**
 *
 * @author kisge
 *
 */
public enum MenuType {
	TOP_MENU, SIDE_MENU
}

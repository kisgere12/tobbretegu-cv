package hu.pte.mik.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.session.SessionFixationProtectionStrategy;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import hu.pte.mik.Constants;
import hu.pte.mik.service.UserService;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;

	private static final String[] AUTH_WHITELIST = { "/index/**", "/css/**", "/js/**", "/image/**", "/login/**",
			"/registration/**", "/h2-console/**", };

	private static final String[] AUTH_WHITELIST_LOGGED = { "/settings/**" };

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(this.userService).passwordEncoder(new BCryptPasswordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/login/**").anonymous().antMatchers(AUTH_WHITELIST).permitAll()
				.antMatchers(AUTH_WHITELIST_LOGGED)
				.hasAnyAuthority(Constants.USER_ROLE_ADMIN, Constants.USER_ROLE_NORMAL).anyRequest().authenticated()
				.and().formLogin().loginPage("/login").loginProcessingUrl("/login")
				.successHandler(new SimpleUrlAuthenticationSuccessHandler());

		http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/index");

		http.exceptionHandling().authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/login"));
		http.sessionManagement().sessionAuthenticationStrategy(new SessionFixationProtectionStrategy());

		http.csrf().disable();
		http.headers().frameOptions().sameOrigin();
	}

}

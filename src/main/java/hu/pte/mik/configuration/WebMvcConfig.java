package hu.pte.mik.configuration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.resource.ResourceUrlProvider;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.UrlTemplateResolver;

import nz.net.ultraq.thymeleaf.LayoutDialect;

@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

	private static final String VIEWS = "/WEB-INF/views/";
	private static final String[] VIEWNAMES = { "*.html" };

	private final ApplicationContext applicationContext;

	public WebMvcConfig(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	public RequestMappingHandlerMapping requestMappingHandlerMapping(
			ContentNegotiationManager contentNegotiationManager, FormattingConversionService conversionService,
			ResourceUrlProvider resourceUrlProvider) {
		RequestMappingHandlerMapping mapping = super.requestMappingHandlerMapping(contentNegotiationManager,
				conversionService, resourceUrlProvider);
		mapping.setUseSuffixPatternMatch(false);
		mapping.setUseTrailingSlashMatch(false);

		return mapping;
	}

	@Bean
	public ITemplateResolver templateResolver() {
		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
		resolver.setApplicationContext(this.applicationContext);
		resolver.setPrefix(VIEWS);
		resolver.setSuffix(".html");
		resolver.setTemplateMode(TemplateMode.HTML);
		resolver.setCharacterEncoding("UTF-8");
		resolver.setCacheable(false);

		return resolver;
	}

	@Bean
	public SpringTemplateEngine templateEngine() {
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.addTemplateResolver(new UrlTemplateResolver());
		templateEngine.addTemplateResolver(this.templateResolver());
		templateEngine.addDialect(new SpringSecurityDialect());
		templateEngine.addDialect(new LayoutDialect());
		templateEngine.addDialect(new Java8TimeDialect());

		return templateEngine;
	}

	@Bean
	public ViewResolver viewResolver() {
		ThymeleafViewResolver thymeleafViewResolver = new ThymeleafViewResolver();
		thymeleafViewResolver.setTemplateEngine(this.templateEngine());
		thymeleafViewResolver.setContentType("text/html");
		thymeleafViewResolver.setViewNames(VIEWNAMES);
		thymeleafViewResolver.setCharacterEncoding("UTF-8");

		return thymeleafViewResolver;
	}

	@Override
	protected void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**", "/css/**").addResourceLocations("/WEB-INF/resources/",
				"/WEB-INF/css/");
		registry.addResourceHandler("/resources/**", "/js/**").addResourceLocations("/WEB-INF/resources/",
				"/WEB-INF/js/");
		registry.addResourceHandler("/resources/**", "/image/**").addResourceLocations("/WEB-INF/resources/",
				"/WEB-INF/image/");
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}
}

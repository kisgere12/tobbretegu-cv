package hu.pte.mik.controller;

import javax.management.relation.RoleNotFoundException;
import javax.validation.Valid;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import hu.pte.mik.model.dto.UserRegistrationDTO;
import hu.pte.mik.service.MenuService;
import hu.pte.mik.service.UserService;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

	private final UserService userService;

	private final MenuService menuService;

	public RegistrationController(UserService userService, MenuService menuService) {
		this.userService = userService;
		this.menuService = menuService;
	}

	@ModelAttribute("user")
	public UserRegistrationDTO userRegistrationDto() {
		return new UserRegistrationDTO();
	}

	@GetMapping
	public String registrationPage(Model model) {
		SecurityContext context = SecurityContextHolder.getContext();
		String username = context.getAuthentication().getName();

		model.addAttribute("username", username);
		model = this.menuService.createMenuStructure(model, context);

		if (!username.equals("anonymousUser")) {
			return "redirect:index";
		}

		return "registration";
	}

	@PostMapping
	public String registration(@ModelAttribute("user") @Valid UserRegistrationDTO userDto, BindingResult result,
			Model model) throws RoleNotFoundException {
		SecurityContext context = SecurityContextHolder.getContext();
		model.addAttribute("username", context.getAuthentication().getName());

		if (this.userService.findByEmail(userDto.getEmail()) != null) {
			result.rejectValue("email", null, "A megadott email címmel már regisztráltak.");
		}
		if (this.userService.findByUsername(userDto.getUsername()) != null) {
			result.rejectValue("username", null, "A megadott felhasználónév foglalt.");
		}
		if (!userDto.getPassword().equals(userDto.getConfirmPassword())) {
			result.rejectValue("confirmPassword", null, "A megadott jelszavak eltérnek egymástól.");
		}
		if (!userDto.getEmail().equals(userDto.getConfirmEmail())) {
			result.rejectValue("confirmEmail", null, "A megadott email címek eltérnek egymástól");
		}

		if (result.hasErrors()) {
			return "registration";
		}

		this.userService.registration(userDto);
		return "redirect:/registration?success";

	}

}

package hu.pte.mik.controller;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import hu.pte.mik.service.MenuService;

@Controller
public class IndexController {

	private final MenuService menuService;

	public IndexController(MenuService menuService) {
		this.menuService = menuService;
	}

	@RequestMapping(value = "/index")
	public String login(Model model) {
		SecurityContext context = SecurityContextHolder.getContext();

		model.addAttribute("username", context.getAuthentication().getName());
		model = this.menuService.createMenuStructure(model, context);

		return "index";
	}

}

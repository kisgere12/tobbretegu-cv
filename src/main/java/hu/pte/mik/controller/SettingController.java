package hu.pte.mik.controller;

import java.util.HashSet;

import javax.validation.Valid;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hu.pte.mik.model.Study;
import hu.pte.mik.model.User;
import hu.pte.mik.model.Work;
import hu.pte.mik.model.dto.StudyDTO;
import hu.pte.mik.model.dto.WorkDTO;
import hu.pte.mik.service.ActivityService;
import hu.pte.mik.service.MenuService;
import hu.pte.mik.service.UserService;

@Controller
public class SettingController {

	private final MenuService menuService;

	private final ActivityService activityService;

	private final UserService userService;

	public SettingController(MenuService menuService, ActivityService activityService, UserService userService) {
		this.menuService = menuService;
		this.activityService = activityService;
		this.userService = userService;
	}

	@RequestMapping(value = "/settings")
	public String setting(Model model) {
		SecurityContext context = SecurityContextHolder.getContext();
		String username = context.getAuthentication().getName();
		User user = null;

		if (username.equals("anonymousUser")) {
			return "redirect:index";

		} else {
			user = (User) context.getAuthentication().getPrincipal();

			user.setStudySet(new HashSet<>(this.activityService.findAllStudyByUserId(user.getUserId())));
			user.setWorkSet(new HashSet<>(this.activityService.findAllWorkByUserId(user.getUserId())));
		}

		model.addAttribute("user", user);
		model.addAttribute("username", username);
		model = this.menuService.createMenuStructure(model, context);

		return "settings";
	}

	@GetMapping(value = "/settings/study")
	public String study(@RequestParam(required = false) Integer studyId, Model model) {
		SecurityContext context = SecurityContextHolder.getContext();
		String username = context.getAuthentication().getName();
		User user = null;
		Study study = null;

		if (username.equals("anonymousUser")) {
			return "redirect:index";
		} else {
			user = (User) context.getAuthentication().getPrincipal();
		}

		if (studyId != null) {
			study = this.activityService.findStudyById(studyId);

			if ((study == null) || !study.getUser().equals(user)) {
				return "redirect:/settings";
			}
		} else {
			study = new Study();
			study.setUser(user);
		}

		model.addAttribute("study", new StudyDTO(study));

		model.addAttribute("username", username);
		model = this.menuService.createMenuStructure(model, context);

		return "settings/study";
	}

	@PostMapping(value = "/settings/study")
	public String saveStudy(@ModelAttribute("study") @Valid StudyDTO studyDTO, BindingResult result, Model model) {
		SecurityContext context = SecurityContextHolder.getContext();
		String username = context.getAuthentication().getName();
		User user = (User) context.getAuthentication().getPrincipal();
		Study study = null;

		if (studyDTO.getId() != null) {
			study = this.activityService.findStudyById(studyDTO.getId());
			if (study == null) {
				result.rejectValue("global", null, "Nem létező tanulmány mentése sikertelen.");
			} else if (user.equals(this.userService.findById(studyDTO.getId()))) {
				result.rejectValue("global", null, "A tanulmány nem ehhez a felhasználóhoz tartozik.");
			}
		}

		if (studyDTO.getEndDate().before(studyDTO.getStartDate())) {
			result.rejectValue("startDate", null, "A tanulmány kezdeti dátuma későbbi, mint befejezési dátum.");
		}

		if (result.hasErrors()) {
			model.addAttribute("username", username);
			model = this.menuService.createMenuStructure(model, context);
			model.addAttribute("study", studyDTO);

			return "settings/study";
		}

		if (study == null) {
			study = new Study();
			study.setUser(user);
		}

		study.setName(studyDTO.getName());
		study.setStartDate(studyDTO.getStartDate());
		study.setEndDate(studyDTO.getEndDate());
		study.setResult(studyDTO.getResult());

		this.activityService.saveStudy(study);

		return "redirect:/settings";
	}

	@GetMapping(value = "/settings/study/delete")
	public String deleteStudy(@RequestParam(required = false) Integer studyId) {
		SecurityContext context = SecurityContextHolder.getContext();
		String username = context.getAuthentication().getName();
		Study study = this.activityService.findStudyById(studyId);

		if (!username.equals("anonymousUser") && (study != null)) {
			User user = (User) context.getAuthentication().getPrincipal();
			if (user.equals(study.getUser())) {
				this.activityService.deleteStudy(study);
			}
		}

		return "redirect:/settings";
	}

	@GetMapping(value = "/settings/work")
	public String work(@RequestParam(required = false) Integer studyId, Model model) {
		SecurityContext context = SecurityContextHolder.getContext();
		String username = context.getAuthentication().getName();
		User user = null;
		Work work = null;

		if (username.equals("anonymousUser")) {
			return "redirect:index";
		} else {
			user = (User) context.getAuthentication().getPrincipal();
		}

		if (studyId != null) {
			work = this.activityService.findWorkById(studyId);

			if ((work == null) || !work.getUser().equals(user)) {
				return "redirect:/settings";
			}
		} else {
			work = new Work();
			work.setUser(user);
		}

		model.addAttribute("work", new WorkDTO(work));

		model.addAttribute("username", username);
		model = this.menuService.createMenuStructure(model, context);

		return "settings/work";
	}

	@PostMapping(value = "/settings/work")
	public String saveWork(@ModelAttribute("work") @Valid WorkDTO workDTO, BindingResult result, Model model) {
		SecurityContext context = SecurityContextHolder.getContext();
		String username = context.getAuthentication().getName();
		User user = (User) context.getAuthentication().getPrincipal();
		Work work = null;

		if (workDTO.getId() != null) {
			work = this.activityService.findWorkById(workDTO.getId());
			if (work == null) {
				result.rejectValue("global", null, "Nem létező tanulmány mentése sikertelen.");
			} else if (user.equals(this.userService.findById(workDTO.getId()))) {
				result.rejectValue("global", null, "A tanulmány nem ehhez a felhasználóhoz tartozik.");
			}
		}

		if (workDTO.getEndDate().before(workDTO.getStartDate())) {
			result.rejectValue("startDate", null, "A tanulmány kezdeti dátuma későbbi, mint befejezési dátum.");
		}

		if (result.hasErrors()) {
			model.addAttribute("username", username);
			model = this.menuService.createMenuStructure(model, context);
			model.addAttribute("work", workDTO);

			return "settings/work";
		}

		if (work == null) {
			work = new Work();
			work.setUser(user);
		}

		work.setName(workDTO.getName());
		work.setStartDate(workDTO.getStartDate());
		work.setEndDate(workDTO.getEndDate());
		work.setPosition(workDTO.getPosition());
		work.setTask(workDTO.getTask());

		this.activityService.saveWork(work);

		return "redirect:/settings";
	}

	@GetMapping(value = "/settings/work/delete")
	public String deleteWork(@RequestParam(required = false) Integer workId) {
		SecurityContext context = SecurityContextHolder.getContext();
		String username = context.getAuthentication().getName();
		Work work = this.activityService.findWorkById(workId);

		if (!username.equals("anonymousUser") && (work != null)) {
			User user = (User) context.getAuthentication().getPrincipal();
			if (user.equals(work.getUser())) {
				this.activityService.deleteWork(work);
			}
		}

		return "redirect:/settings";
	}
}

package hu.pte.mik.controller;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hu.pte.mik.service.MenuService;

@Controller
public class LoginController {

	private final MenuService menuService;

	public LoginController(MenuService menuService) {
		this.menuService = menuService;
	}

	@RequestMapping(value = "/login")
	public String login(@RequestParam(required = false) String error, Model model) {
		SecurityContext context = SecurityContextHolder.getContext();
		String username = context.getAuthentication().getName();

		model.addAttribute("error", error);
		model.addAttribute("username", username);
		model = this.menuService.createMenuStructure(model, context);

		if (!username.equals("anonymousUser")) {
			return "redirect:index";
		}
		return "login";
	}

}

package hu.pte.mik.service;

import java.util.List;

import javax.management.relation.RoleNotFoundException;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import hu.pte.mik.Constants;
import hu.pte.mik.dao.UserDao;
import hu.pte.mik.dao.UserRoleDao;
import hu.pte.mik.model.User;
import hu.pte.mik.model.dto.UserRegistrationDTO;

/**
 *
 * @author kisge
 *
 */
@Service
public class UserService implements UserDetailsService {

	private final UserDao userDao;

	private final UserRoleDao userRoleDao;

	private final PasswordEncoder encoder;

	public UserService(UserDao userDao, UserRoleDao userRoleDao) {
		this.userDao = userDao;
		this.userRoleDao = userRoleDao;
		this.encoder = new BCryptPasswordEncoder();
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = this.userDao.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User not found"));
		return user;
	}

	public List<User> findAll() {
		return this.userDao.findAll();
	}

	public User findById(Integer userId) {
		return this.userDao.findById(userId).orElse(null);
	}

	public User findByEmail(String email) {
		return this.userDao.findByEmail(email).orElse(null);
	}

	public User save(User user) {
		return this.userDao.save(user);
	}

	public User findByUsername(String username) {
		return this.userDao.findByUsername(username).orElse(null);
	}

	public User registration(UserRegistrationDTO userDto) throws RoleNotFoundException {
		return this.save(new User(userDto.getUsername(), this.encoder.encode(userDto.getPassword()), userDto.getEmail(),
				this.userRoleDao.findByAuthority(Constants.USER_ROLE_NORMAL)
						.orElseThrow(() -> new RoleNotFoundException("Role not found")),
				userDto.getPlaceOfBirth(), userDto.getDateOfBirth(), userDto.getNationality(),
				userDto.getPhoneNumber()));
	}

}

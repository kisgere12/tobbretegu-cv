package hu.pte.mik.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import hu.pte.mik.dao.MenuDao;
import hu.pte.mik.model.User;
import hu.pte.mik.model.UserRole;
import hu.pte.mik.model.menu.MenuItem;
import hu.pte.mik.model.menu.MenuType;

/**
 *
 * @author kisge
 */
@Service
public class MenuService {

	private final MenuDao menuDao;

	public MenuService(MenuDao menuDao) {
		this.menuDao = menuDao;
	}

	/**
	 * Returns all instances of the type.
	 *
	 * @return all entities
	 */
	public List<MenuItem> findAll() {
		return this.menuDao.findAll();
	}

	/**
	 * Retrieves an entity by its id.
	 *
	 * @param id must not be null.
	 * @return the entity with the given id or null if none found.
	 */
	public MenuItem findById(Integer id) {
		return this.menuDao.findById(id).orElse(null);
	}

	/**
	 * Retrieves all entity by its role where parent menu item is null.
	 *
	 * @param roleId must not be null.
	 * @return entity list with the given roleId or an empty list if none found.
	 */
	public List<MenuItem> findByRole(Integer roleId) {
		return this.menuDao.findByRoleIdAndParentMenuItemIsNull(roleId);
	}

	/**
	 * Retrieves all entity by its role where parent menu item is null.
	 *
	 * @param roleList must not be null.
	 * @return entity list with the given role or an empty list if none found.
	 */
	public List<MenuItem> findByRoleList(List<GrantedAuthority> roleList) {
		return this.menuDao.findByRoleInAndParentMenuItemIsNull(roleList);
	}

	/**
	 * Retrieves all entity by its role where parent menu item is null.
	 *
	 * @param role must not be null.
	 * @return entity list with the given role or an empty list if none found.
	 */
	public List<MenuItem> findByRole(UserRole role) {
		return this.menuDao.findByRoleAndParentMenuItemIsNull(role);
	}

	/**
	 * Retrieves all entity by its role.
	 *
	 * @param type must not be null.
	 * @return entity list with the given type or an empty list if none found.
	 */
	public List<MenuItem> findByMenuType(MenuType type) {
		return this.menuDao.findByMenuType(type);
	}

	/**
	 * Retrieves all entity by its role and type where parent menu item is null.
	 *
	 * @param roleList must not be null.
	 * @param type     must not be null.
	 * @return entity list with the given role and type or an empty list if none
	 *         found.
	 */
	public List<MenuItem> findByRoleListAndMenuType(List<GrantedAuthority> roleList, MenuType type) {
		return this.menuDao.findByRoleInAndMenuTypeAndParentMenuItemIsNullOrderByOrder(roleList, type);
	}

	/**
	 * Returns all instances of the type where parent menu item is null.
	 *
	 * @return entity list.
	 */
	public List<MenuItem> findMainMenus() {
		return this.menuDao.findByParentMenuItemIsNull();
	}

	@SuppressWarnings("unchecked")
	public Model createMenuStructure(Model model, SecurityContext context) {
		List<GrantedAuthority> userRoles = (context.getAuthentication().getPrincipal() instanceof User)
				? new ArrayList<>(((User) context.getAuthentication().getPrincipal()).getAuthorities())
				: Collections.EMPTY_LIST;

		model.addAttribute("menu", this.findByRoleListAndMenuType(userRoles, MenuType.TOP_MENU));
		model.addAttribute("sideMenu", this.findByRoleListAndMenuType(userRoles, MenuType.SIDE_MENU));

		return model;
	}

}

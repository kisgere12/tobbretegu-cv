package hu.pte.mik.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hu.pte.mik.dao.StudyDao;
import hu.pte.mik.dao.WorkDao;
import hu.pte.mik.model.Study;
import hu.pte.mik.model.Work;

@Service
public class ActivityService {

	private final StudyDao studyDao;

	private final WorkDao workDao;

	public ActivityService(StudyDao studyDao, WorkDao workdao) {
		this.studyDao = studyDao;
		this.workDao = workdao;
	}

	public Study findStudyById(Integer id) {
		return this.studyDao.findById(id).orElse(null);
	}

	public List<Study> findAllStudyByUserId(Integer userId) {
		return this.studyDao.findByUserUserIdOrderByStartDateAscIdAsc(userId);
	}

	public Study saveStudy(Study study) {
		return this.studyDao.save(study);
	}

	public void deleteStudy(Study study) {
		this.studyDao.delete(study);
	}

	public Work findWorkById(Integer id) {
		return this.workDao.findById(id).orElse(null);
	}

	public List<Work> findAllWorkByUserId(Integer userId) {
		return this.workDao.findByUserUserIdOrderByStartDateAscIdAsc(userId);
	}

	public Work saveWork(Work work) {
		return this.workDao.save(work);
	}

	public void deleteWork(Work work) {
		this.workDao.delete(work);
	}
}

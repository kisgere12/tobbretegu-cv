package hu.pte.mik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TobbreteguCvApplication {

	public static void main(String[] args) {
		SpringApplication.run(TobbreteguCvApplication.class, args);
	}

}

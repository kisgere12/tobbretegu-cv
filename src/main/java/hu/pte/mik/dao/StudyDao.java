package hu.pte.mik.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.pte.mik.model.Study;

public interface StudyDao extends JpaRepository<Study, Integer> {
	public List<Study> findByUserUserIdOrderByStartDateAscIdAsc(Integer userId);
}

package hu.pte.mik.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.GrantedAuthority;

import hu.pte.mik.model.UserRole;
import hu.pte.mik.model.menu.MenuItem;
import hu.pte.mik.model.menu.MenuType;

/**
 *
 * @author kisge
 *
 */
public interface MenuDao extends JpaRepository<MenuItem, Integer> {

	/**
	 * Retrieves an entity by its id.
	 *
	 * @param id must not be null.
	 * @return the entity with the given id or null if none found.
	 */
	public List<MenuItem> findByRoleId(Integer roleId);

	/**
	 * Retrieves all entity by its role where parent menu item is null.
	 *
	 * @param role must not be null.
	 * @return entity list with the given role or an empty list if none found.
	 */
	public List<MenuItem> findByRole(UserRole role);

	/**
	 * Retrieves all entity by its role.
	 *
	 * @param type must not be null.
	 * @return entity list with the given type or an empty list if none found.
	 */
	public List<MenuItem> findByMenuType(MenuType type);

	/**
	 * Returns all instances of the type where parent menu item is null.
	 *
	 * @return entity list.
	 */
	public List<MenuItem> findByParentMenuItemIsNull();

	/**
	 * Retrieves an entity by its id.
	 *
	 * @param id must not be null.
	 * @return the entity with the given id or null if none found.
	 */
	public List<MenuItem> findByRoleIdAndParentMenuItemIsNull(Integer roleId);

	/**
	 * Retrieves all entity by its role where parent menu item is null.
	 *
	 * @param role must not be null.
	 * @return entity list with the given role or an empty list if none found.
	 */
	public List<MenuItem> findByRoleAndParentMenuItemIsNull(UserRole role);

	/**
	 * Retrieves all entity by its role where parent menu item is null.
	 *
	 * @param roleList must not be null.
	 * @return entity list with the given role or an empty list if none found.
	 */
	public List<MenuItem> findByRoleInAndParentMenuItemIsNull(List<GrantedAuthority> roleList);

	/**
	 * Retrieves all entity by its role and type where parent menu item is null.
	 *
	 * @param roleList must not be null.
	 * @param type     must not be null.
	 * @return entity list with the given role and type or an empty list if none
	 *         found.
	 */
	public List<MenuItem> findByRoleInAndMenuTypeAndParentMenuItemIsNullOrderByOrder(List<GrantedAuthority> roleList,
			MenuType type);

}

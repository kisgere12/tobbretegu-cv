package hu.pte.mik.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.pte.mik.model.UserRole;

public interface UserRoleDao extends JpaRepository<UserRole, Integer> {

	public Optional<UserRole> findByAuthority(String authority);

}

package hu.pte.mik.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.pte.mik.model.User;

public interface UserDao extends JpaRepository<User, Integer> {

	public Optional<User> findByUsername(String username);

	public Optional<User> findByEmail(String email);

}

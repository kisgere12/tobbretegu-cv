package hu.pte.mik.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.pte.mik.model.Work;

public interface WorkDao extends JpaRepository<Work, Integer> {
	public List<Work> findByUserUserIdOrderByStartDateAscIdAsc(Integer userId);
}

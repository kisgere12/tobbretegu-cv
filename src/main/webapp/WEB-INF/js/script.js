$(function () {
    $('.sidenav').sidenav();
    $(".dropdown-trigger").dropdown({ hover: true, draggable: true });
    $('.tooltipped').tooltip();
    $('.collapsible').collapsible();
    $('.datepicker').datepicker({ maxDate: new Date(), showMonthAfterYear: false, firstDay: 1, format: 'yyyy.mm.dd', i18n: createDatePickerI18N(), yearRange: [new Date().getFullYear() - 100, new Date().getFullYear()] });

    $('select').formSelect();

    $(window).resize(e => resizeEventHandler());

    swapCheckBox();
});

function resizeEventHandler() {
    // Removing opened sidenav at 993 px
    $searchForm = $("#resp-nav");
    $transform = $searchForm.css("transform").replace("(", "").replace(")", "").split(", ");
    if ($(window).width() >= 993 && $transform[4] == "0") {
        M.Sidenav.getInstance($searchForm).close();
    }
}

function swapCheckBox() {
    $("form").find("div.form-group").each(function () {
        $(this).find(":checkbox").each(function () {
            $hidden = $(this).next(":hidden");
            if ($hidden.length > 0) {
                $hidden.after(this);
            }
        });
    });
}

function createDatePickerI18N() {
    return {
        cancel: "Vissza",
        clear: "Törlés",
        months: ["Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"],
        monthsShort: ["Jan", "Feb", "Már", "Ápr", "Máj", "Jún", "Júl", "Aug", "Szep", "Okt", "Nov", "Dec"],
        weekdays: ["Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat", "Vasárnap"],
        weekdaysShort: ["H", "K", "Sze", "Cs", "P", "Szo", "V"],
        weekdaysAbbrev: ['M', 'T', 'W', 'T', 'F', 'S', 'S']
    };
}
# Többrétegű alkalmazásfejlesztés - CV

### Technologies
* [Spring](https://spring.io/) - Application framework
* [Thymeleaf](https://www.thymeleaf.org/) - Template engine
* [Materialize](https://materializecss.com/) - Material Design
* [Bootstrap](https://getbootstrap.com/) - Responsive css
* [SCSS/SASS](https://sass-lang.com/) - Design
* [H2](https://www.h2database.com/html/main.html) - Database
* [Hibernate](https://hibernate.org/) - ORM

### Plugins
| Plugin | README |
| ------ | ------ |
| SASS Maven Plugin | [Github](https://github.com/GeoDienstenCentrum/sass-maven-plugin/blob/master/README.md) |

### Compile SCSS to CSS
```
mvn sass:watch
```
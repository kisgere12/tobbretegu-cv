;             
CREATE USER IF NOT EXISTS "SA" SALT '996947c9427ef32d' HASH 'db4573de37ec047cc4feb082bfb65101e9ea2ad7642d9d8227b29c61a6473735' ADMIN;         
CREATE SEQUENCE "PUBLIC"."SYSTEM_SEQUENCE_E12F7B9A_AD72_404D_B2A2_2E2330412E7F" START WITH 3 BELONGS_TO_TABLE;
CREATE SEQUENCE "PUBLIC"."SYSTEM_SEQUENCE_C7850FC9_5F7F_4312_8A1A_6086ECBE3A65" START WITH 3 BELONGS_TO_TABLE;
CREATE SEQUENCE "PUBLIC"."SYSTEM_SEQUENCE_50AF4D3D_D5A6_4D8E_B873_B19F4F9490EA" START WITH 3 BELONGS_TO_TABLE;
CREATE SEQUENCE "PUBLIC"."SYSTEM_SEQUENCE_3A5A622D_A65E_4C3A_9B98_72471A4AB4BC" START WITH 3 BELONGS_TO_TABLE;
CREATE CACHED TABLE "PUBLIC"."T_ROLE"(
    "ID" INTEGER DEFAULT NEXT VALUE FOR "PUBLIC"."SYSTEM_SEQUENCE_3A5A622D_A65E_4C3A_9B98_72471A4AB4BC" NOT NULL NULL_TO_DEFAULT SEQUENCE "PUBLIC"."SYSTEM_SEQUENCE_3A5A622D_A65E_4C3A_9B98_72471A4AB4BC",
    "AUTHORITY" VARCHAR(100) NOT NULL
); 
ALTER TABLE "PUBLIC"."T_ROLE" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_9" PRIMARY KEY("ID");       
-- 2 +/- SELECT COUNT(*) FROM PUBLIC.T_ROLE;  
INSERT INTO "PUBLIC"."T_ROLE" VALUES
(1, 'ADMIN'),
(2, 'NORMAL');           
CREATE CACHED TABLE "PUBLIC"."T_STUDY"(
    "ID" INTEGER DEFAULT NEXT VALUE FOR "PUBLIC"."SYSTEM_SEQUENCE_50AF4D3D_D5A6_4D8E_B873_B19F4F9490EA" NOT NULL NULL_TO_DEFAULT SEQUENCE "PUBLIC"."SYSTEM_SEQUENCE_50AF4D3D_D5A6_4D8E_B873_B19F4F9490EA",
    "END" TIMESTAMP NOT NULL,
    "NAME" VARCHAR(255) NOT NULL,
    "START" TIMESTAMP NOT NULL,
    "RESULT" VARCHAR(255) NOT NULL,
    "USER_ID" INTEGER NOT NULL
);               
ALTER TABLE "PUBLIC"."T_STUDY" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_2" PRIMARY KEY("ID");      
-- 2 +/- SELECT COUNT(*) FROM PUBLIC.T_STUDY; 
INSERT INTO "PUBLIC"."T_STUDY" VALUES
(1, TIMESTAMP '2020-01-12 00:00:00', STRINGDECODE('Tanulm\u00e1ny1'), TIMESTAMP '2019-10-01 00:00:00', STRINGDECODE('Eredm\u00e9ny1'), 2),
(2, TIMESTAMP '2020-01-12 00:00:00', STRINGDECODE('Tanulm\u00e1ny2'), TIMESTAMP '2019-10-01 00:00:00', STRINGDECODE('Eredm\u00e9ny2'), 2); 
CREATE CACHED TABLE "PUBLIC"."T_USER"(
    "ID" INTEGER DEFAULT NEXT VALUE FOR "PUBLIC"."SYSTEM_SEQUENCE_C7850FC9_5F7F_4312_8A1A_6086ECBE3A65" NOT NULL NULL_TO_DEFAULT SEQUENCE "PUBLIC"."SYSTEM_SEQUENCE_C7850FC9_5F7F_4312_8A1A_6086ECBE3A65",
    "ACCOUNT_NON_EXPIRED" BOOLEAN NOT NULL,
    "ACCOUNT_NON_LOCKED" BOOLEAN NOT NULL,
    "CREATED_ON" TIMESTAMP NOT NULL,
    "CREDENTIALS_NON_EXPIRED" BOOLEAN NOT NULL,
    "DATE_OF_BIRTH" TIMESTAMP NOT NULL,
    "EMAIL" VARCHAR(255) NOT NULL,
    "ENABLED" BOOLEAN NOT NULL,
    "NATIONALITY" VARCHAR(255) NOT NULL,
    "PASSWORD" VARCHAR(255) NOT NULL,
    "PHONE_NUMBER" VARCHAR(255) NOT NULL,
    "PLACE_OF_BIRTH" VARCHAR(255) NOT NULL,
    "USERNAME" VARCHAR(255) NOT NULL
);           
ALTER TABLE "PUBLIC"."T_USER" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_94" PRIMARY KEY("ID");      
-- 2 +/- SELECT COUNT(*) FROM PUBLIC.T_USER;  
INSERT INTO "PUBLIC"."T_USER" VALUES
(1, TRUE, TRUE, TIMESTAMP '2020-01-13 20:58:45.86', TRUE, TIMESTAMP '1997-02-15 00:00:00', 'teszt@teszt.hu', TRUE, 'magyar', '$2a$10$PoyKXQkBKVULtFOa1N4TnuhmlGJyl0zXk4M0Si9n6hCNXL7MIKuO2', '123456789', STRINGDECODE('P\u00e9cs'), 'admin'),
(2, TRUE, TRUE, TIMESTAMP '2020-01-13 21:02:54.559', TRUE, TIMESTAMP '1997-02-15 00:00:00', 'teszt2@teszt.hu', TRUE, STRINGDECODE('Magyarorsz\u00e1g'), '$2a$10$SBaXKeeOcfzbFDdOtXmIouPbbHZx.9.QvpXlkaG0YDe.ySk5yrss.', '987654321', STRINGDECODE('P\u00e9cs'), 'normal');              
CREATE CACHED TABLE "PUBLIC"."T_USER_ROLE"(
    "USER_ID" INTEGER NOT NULL,
    "ROLE_ID" INTEGER NOT NULL
);              
ALTER TABLE "PUBLIC"."T_USER_ROLE" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_3" PRIMARY KEY("USER_ID", "ROLE_ID");  
-- 2 +/- SELECT COUNT(*) FROM PUBLIC.T_USER_ROLE;             
INSERT INTO "PUBLIC"."T_USER_ROLE" VALUES
(1, 1),
(2, 2);   
CREATE CACHED TABLE "PUBLIC"."T_WORK"(
    "ID" INTEGER DEFAULT NEXT VALUE FOR "PUBLIC"."SYSTEM_SEQUENCE_E12F7B9A_AD72_404D_B2A2_2E2330412E7F" NOT NULL NULL_TO_DEFAULT SEQUENCE "PUBLIC"."SYSTEM_SEQUENCE_E12F7B9A_AD72_404D_B2A2_2E2330412E7F",
    "END" TIMESTAMP NOT NULL,
    "NAME" VARCHAR(255) NOT NULL,
    "START" TIMESTAMP NOT NULL,
    "POSITION" VARCHAR(255) NOT NULL,
    "TASK" CLOB NOT NULL,
    "USER_ID" INTEGER NOT NULL
);   
ALTER TABLE "PUBLIC"."T_WORK" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_94BA" PRIMARY KEY("ID");    
-- 2 +/- SELECT COUNT(*) FROM PUBLIC.T_WORK;  
INSERT INTO "PUBLIC"."T_WORK" VALUES
(1, TIMESTAMP '2020-01-12 00:00:00', 'Munka1', TIMESTAMP '2019-10-01 00:00:00', STRINGDECODE('Poz\u00edci\u00f31'), STRINGDECODE('Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae rutrum justo. Proin blandit, justo in sodales tincidunt, augue libero convallis turpis, nec fringilla turpis justo ut erat. Donec rutrum tristique nisl quis efficitur. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer sapien dui, gravida nec vestibulum eu, elementum ac nisl. Fusce eget augue in augue eleifend fermentum quis in mi. Etiam lacus nisl, posuere eget rhoncus in, dignissim in urna. Phasellus condimentum neque nec lacus rutrum egestas non quis dui. Phasellus accumsan diam a viverra imperdiet. Sed sed neque dignissim, iaculis ligula et, venenatis odio. Nulla posuere sed orci eget varius.\r\n\r\nVivamus mollis purus id neque elementum pulvinar eget id velit. Ut vel ultrices ex, eget tincidunt sem. Cras auctor, odio at pretium pellentesque, neque nulla gravida justo, a pretium nunc arcu ultrices orci. Cras dapibus varius libero, pharetra congue sapien viverra vitae. Cras sollicitudin iaculis nunc, convallis interdum tortor pulvinar vel. Maecenas vulputate convallis viverra. Nam et nunc vel dui posuere lobortis sit amet sit amet urna.\r\n\r\nNulla facilisi. Etiam congue justo eros, in iaculis est ultrices a. Maecenas dictum, erat vel pellentesque blandit, nisl tortor bibendum sapien, nec tristique lorem dui non purus. Sed ut dui metus. Aliquam eget vestibulum arcu. Aliquam augue justo, consequat vel dui quis, euismod volutpat ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent eget vehicula lorem. Nam faucibus purus vitae felis euismod ultrices.\r\n\r\nMauris porta lobortis est quis aliquam. Mauris laoreet nec lacus et gravida. Cras rhoncus libero vehicula, laoreet tortor eu, feugiat velit. Aenean porta eleifend auctor. Vestibulum posuere aliquet ipsum, eget mattis lacus malesuada at. Mauris pretium neque diam, vitae convallis metus scelerisque vel. Sed suscipit, magna eget vulputate mollis, diam velit pulvinar mi, id bibendum enim lorem ac justo. Proin dignissim laoreet tortor. Quisque porta porttitor consectetur.\r\n'), 2),
(2, TIMESTAMP '2020-01-12 00:00:00', 'Munka2', TIMESTAMP '2019-10-01 00:00:00', STRINGDECODE('Poz\u00edci\u00f32'), STRINGDECODE('Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae rutrum justo. Proin blandit, justo in sodales tincidunt, augue libero convallis turpis, nec fringilla turpis justo ut erat. Donec rutrum tristique nisl quis efficitur. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer sapien dui, gravida nec vestibulum eu, elementum ac nisl. Fusce eget augue in augue eleifend fermentum quis in mi. Etiam lacus nisl, posuere eget rhoncus in, dignissim in urna. Phasellus condimentum neque nec lacus rutrum egestas non quis dui. Phasellus accumsan diam a viverra imperdiet. Sed sed neque dignissim, iaculis ligula et, venenatis odio. Nulla posuere sed orci eget varius.\r\n\r\nVivamus mollis purus id neque elementum pulvinar eget id velit. Ut vel ultrices ex, eget tincidunt sem. Cras auctor, odio at pretium pellentesque, neque nulla gravida justo, a pretium nunc arcu ultrices orci. Cras dapibus varius libero, pharetra congue sapien viverra vitae. Cras sollicitudin iaculis nunc, convallis interdum tortor pulvinar vel. Maecenas vulputate convallis viverra. Nam et nunc vel dui posuere lobortis sit amet sit amet urna.\r\n\r\nNulla facilisi. Etiam congue justo eros, in iaculis est ultrices a. Maecenas dictum, erat vel pellentesque blandit, nisl tortor bibendum sapien, nec tristique lorem dui non purus. Sed ut dui metus. Aliquam eget vestibulum arcu. Aliquam augue justo, consequat vel dui quis, euismod volutpat ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent eget vehicula lorem. Nam faucibus purus vitae felis euismod ultrices.\r\n\r\nMauris porta lobortis est quis aliquam. Mauris laoreet nec lacus et gravida. Cras rhoncus libero vehicula, laoreet tortor eu, feugiat velit. Aenean porta eleifend auctor. Vestibulum posuere aliquet ipsum, eget mattis lacus malesuada at. Mauris pretium neque diam, vitae convallis metus scelerisque vel. Sed suscipit, magna eget vulputate mollis, diam velit pulvinar mi, id bibendum enim lorem ac justo. Proin dignissim laoreet tortor. Quisque porta porttitor consectetur.'), 2);            
CREATE CACHED TABLE "PUBLIC"."T_MENU_ROLE"(
    "MENU_ID" INTEGER NOT NULL,
    "ROLE_ID" INTEGER NOT NULL
);              
-- 2 +/- SELECT COUNT(*) FROM PUBLIC.T_MENU_ROLE;             
INSERT INTO "PUBLIC"."T_MENU_ROLE" VALUES
(1, 1),
(1, 2);   
CREATE CACHED TABLE "PUBLIC"."T_MENU"(
    "ID" INTEGER NOT NULL,
    "MENU_TYPE" VARCHAR(255) NOT NULL,
    "NAME" VARCHAR(255) NOT NULL,
    "MENU_ORDER" INTEGER NOT NULL,
    "URL" VARCHAR(255) NOT NULL,
    "PARENT_MENU_ID" INTEGER
);         
ALTER TABLE "PUBLIC"."T_MENU" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_94B" PRIMARY KEY("ID");     
-- 1 +/- SELECT COUNT(*) FROM PUBLIC.T_MENU;  
INSERT INTO "PUBLIC"."T_MENU" VALUES
(1, 'TOP_MENU', STRINGDECODE('Be\u00e1ll\u00edt\u00e1sok'), 1, 'settings', NULL);       
ALTER TABLE "PUBLIC"."T_MENU_ROLE" ADD CONSTRAINT "PUBLIC"."UK_F8T9YI5TY3UT4YOHCD2B7G3U8" UNIQUE("ROLE_ID");  
ALTER TABLE "PUBLIC"."T_USER_ROLE" ADD CONSTRAINT "PUBLIC"."FKA9C8IIY6UT0GNX491FQX4PXAM" FOREIGN KEY("ROLE_ID") REFERENCES "PUBLIC"."T_ROLE"("ID") NOCHECK;   
ALTER TABLE "PUBLIC"."T_STUDY" ADD CONSTRAINT "PUBLIC"."FK44OK128M0X8NTCQRCXNGKQIPV" FOREIGN KEY("USER_ID") REFERENCES "PUBLIC"."T_USER"("ID") NOCHECK;       
ALTER TABLE "PUBLIC"."T_MENU_ROLE" ADD CONSTRAINT "PUBLIC"."FK2OQ55U7E3JL15IMUMS526YDXV" FOREIGN KEY("ROLE_ID") REFERENCES "PUBLIC"."T_ROLE"("ID") NOCHECK;   
ALTER TABLE "PUBLIC"."T_MENU" ADD CONSTRAINT "PUBLIC"."FKQP5X7X14J63J4ORVVB3NTWW22" FOREIGN KEY("PARENT_MENU_ID") REFERENCES "PUBLIC"."T_MENU"("ID") NOCHECK; 
ALTER TABLE "PUBLIC"."T_USER_ROLE" ADD CONSTRAINT "PUBLIC"."FKQ5UN6X7ECOEF5W1N39COP66KL" FOREIGN KEY("USER_ID") REFERENCES "PUBLIC"."T_USER"("ID") NOCHECK;   
ALTER TABLE "PUBLIC"."T_MENU_ROLE" ADD CONSTRAINT "PUBLIC"."FK3QKYV4X44EU60WOB5UM0MCM11" FOREIGN KEY("MENU_ID") REFERENCES "PUBLIC"."T_MENU"("ID") NOCHECK;   
ALTER TABLE "PUBLIC"."T_WORK" ADD CONSTRAINT "PUBLIC"."FKUMJ4WFS53FWXINBQ4IPWB63I" FOREIGN KEY("USER_ID") REFERENCES "PUBLIC"."T_USER"("ID") NOCHECK;         
